#!/usr/bin/env python
#
# Adapted from script by Diana MacLean 2011
#
# Adapted fro CS448G from Michael Noll's tutorial on : http://www.michael-noll.com/tutorials/writing-an-hadoop-mapreduce-program-in-python/
#
#

from operator import itemgetter
import sys

# maps words to their counts
allLines = []

# input comes from STDIN
for line in sys.stdin:
    line.strip()
    div = line.split('\t',2)
    div[2] = int(div[2])
    allLines.append(div)

allLines.sort(key=lambda x: (x[0], x[2]))

# write the results to STDOUT (standard output)
for line in allLines:
    print '%s\t%s\t%s'% (line[0], line[1], line[2])