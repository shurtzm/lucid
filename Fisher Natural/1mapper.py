#!/usr/bin/python
#
# Adapted from script by Diana MacLean 2011
#
# Mapper script adapted for CS448G from Amazon's example: http://aws.amazon.com/jobflows/2273?_encoding=UTF8&jiveRedirect=1
#
#

import sys
import re

def main(argv):
    line = sys.stdin.readline()
    #pattern = re.compile("[a-zA-Z][a-zA-Z0-9]*")
    try:
        while line:
            current = line.split(",")
            hist_action = "*."
            print "*" + "\t" + current[0] + "\t" + current[1]
            for action in current[2].split(".",2):
                print  (hist_action + action).strip() + "\t" + current[0] + "\t" + current[1]
                hist_action += action + "."
            line =  sys.stdin.readline()
    except "end of file":
        return None
if __name__ == "__main__":
    main(sys.argv)