import sys
import json

results = {}
results["left"] = {}
results["right"] = {}

for line in sys.stdin:
    s = line.split(" => ")
    a = s[0][1:-1].split(", ")
    c = s[1].split(", ")[0].strip()[1:-1]
    confidence = s[1].split(", ")[1].strip()

    if not isinstance(a, basestring):
        for i in a:
            if i not in results["left"]:
                results["left"][i]=[]
            results["left"][i].append(line.strip())
    else:
        if a not in results["left"]:
            results["left"][a]=[]
        results["left"][a].append(line.strip())

    if not isinstance(c, basestring):
        for i in c:
            if i not in results["right"]:
                results["right"][i]=[]
            results["right"][i].append(line.strip())
    else:
        if c not in results["right"]:
            results["right"][c]=[]
        results["right"][c].append(line.strip())

print json.dumps(results, ensure_ascii=False)
