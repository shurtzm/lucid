#!/usr/bin/env python
#
# Adapted from script by Diana MacLean 2011
#
# Adapted fro CS448G from Michael Noll's tutorial on : http://www.michael-noll.com/tutorials/writing-an-hadoop-mapreduce-program-in-python/
#
#

from operator import itemgetter
import sys

# maps words to their counts
list = {}

# input comes from STDIN
for line in sys.stdin:
    line.strip()
    div = line.split(', ',7)
    list[div[0]] = [float(div[1]),float(div[2]),float(div[3]),float(div[4]),float(div[5]),float(div[6])]

with open('output3.tsv') as f:
    for line in f:
        user = line.split('\t', 3)
        current = list.get(user[0])
        if current == None:
            continue
        level = 0
        if float(user[2]) >= current[4]:
            #5
            level = 5
        elif float(user[2]) >= current[3]:
            #4
            level = 4
        elif float(user[2]) >= current[2]:
            #3
            level = 3
        elif float(user[2]) >= current[1]:
            #2
            level = 2
        elif float(user[2]) >= current[0]:
            #1
            level = 1
        else:
            #0
            level = 0
        print '%s,%s,%s,%s'% (user[0].strip(), user[1].strip(), user[2].strip(), level)