import sys

file = open(sys.argv[1])
out = open(sys.argv[2], 'w+')
for line in file:
    user_data = line.strip().split(',')
    if user_data[0] == 'User ID':
	     continue
    key = user_data[1]
    numLibraries = 0
    user_data.pop(0)
    user_data.pop(0)
    for activity in user_data:
        if(float(activity) > 0):
            numLibraries += 1
    line = key + ',' + str(numLibraries) + '\n'
    out.write(line)
file.close()
out.close()