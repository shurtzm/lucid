#!/usr/bin/env python

from operator import itemgetter
import sys

premium_libs = ["android",
                "iOS6iPadMockups",
                "iOS6iPhoneMockups",
                "ios",
                "ipad",
                "iphone",
                "ui2",
                "UIMockups",
                "sitemap"
                ]
levels = {
    "free":[1],
    "personal":[2,3,28,42],
    "professional":[4,5,43],
    "team": [6,7,8,9,10,11,12,13,14,15,16,17,28,29,20,21,22,23,24,25,26,27,29,30,31,32,33,34,35,39,40]
}
master = []
users = {}
for line in sys.stdin:
    line = line.strip()
    data = line.split(',')
    if data[1] not in master:
        master.append(data[1])
    index = master.index(data[1])
    level = ""
    for key,value in levels.iteritems():
        if int(data[2]) in value:
            level = key
    if level == "":
        continue
    if level == "free" and data[1] in premium_libs:
        continue
    if data[1] == "seconds_editing":
        continue
    key = data[0] + "," + str(level)
    if key in users:
        users[key].extend([0] * (len(master) - len(users[key])))
        users[key][index] = data[3]
    else:
        users[key] = [0] * len(master)
        users[key][index] = data[3]

for key,value in users.iteritems():
    print key.split(",")[1] + ',' + ','.join(master[key] for key,x in enumerate(value) if x>0)