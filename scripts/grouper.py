#!/usr/bin/env python

from operator import itemgetter
import sys

# maps words to their counts
libraries = [#"android",
             # "aws",
             # "azure",
             # "bpmn",
             # "CircuitDiagrams",
             # "cisco",
             # "DataFlow",
             # "default",
             # "dfd",
             # "ee",
             # "enterprise",
             # "EntityRelationship",
             # "equation",
             # "erd",
             # "floorplan",
             # "flowchart",
             # "GeometricShapes",
             # "iOS6iPadMockups",
             # "iOS6iPhoneMockups",
             # "ios",
             # "ipad",
             # "iphone",
             # "mindmap",
             # "network",
             # "orgchart",
             # "process",
             # "serverrack",
             # "shapes",
             # "sitemap",
             # "table",
             # "techclipart",
             # "ui2",
             # "UIMockups",
             # "uml",
             # "userimage",
             # "valuestream",
             # "venn",
             # "video",
             # "create",
             # "seconds_editing",
             # "shapemanager",
             # "custom",
             "import"]
output = {}
for line in sys.stdin:
    div = line.split(',')
    if len(div)>1:
        parts = div[1].split('.')
        if parts[0] == "press":
            continue
        new_line = div[0] + '\t'
        for word in parts:
            found = False
            new_line += word.strip()
            for book in libraries:
                if word.lower().strip().startswith(book.lower()):
                    if book in output:
                        output[book].append(div[0])
                    else:
                        output[book] = [div[0]]
                    # print new_line
                    found = True
                    break
            # if word.strip() in libraries:
            #     print new_line
            #     break
            if found:
                break
            else:
                new_line+='.'
for key,value in output.iteritems():
    print key+","+",".join(value)
