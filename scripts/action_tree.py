#!/usr/bin/env python

from operator import itemgetter
import sys

class Node:

    def __init__(self,name):
        self.name = name
        self.children = []
        self.children_names = []
        self.ids = []
    def add_id(self, id):
        self.ids.append(id)
    def add_child(self, node):
        self.children.append(node)
        self.children_names.append(node.name)
    def get_child(self, index):
        return self.children[index]
    def get_ids(self):
        return self.ids
    def __contains__(self, item):
        return item in self.children_names
    def get_index(self,item):
        return self.children_names.index(item)
    def print_out(self,tabs):
        print tabs + self.name + '\t' + ",".join(self.ids)
        tabs = tabs + '\t'
        for child in self.children:
            child.print_out(tabs)

tree = Node("root")
for line in open("action_table.csv"):
    line = line.strip()
    id = line.split(',')[0]
    name = line.split(',')[1]
    name = name.strip('"')
    parts = name.split('.')
    subtree = tree
    # print len(tree.get_ids())
    for part in parts:
        if not subtree.__contains__(part):
            new_Node = Node(part)
            subtree.add_child(new_Node)
        subtree = subtree.get_child(subtree.get_index(part))
        subtree.add_id(id)
print ",".join(tree.children_names)
print len(tree.children)
# tree.print_out("")

