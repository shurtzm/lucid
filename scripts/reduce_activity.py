#!/usr/bin/env python

from operator import itemgetter
import sys

# Print out user, library, count, and daycount
user_library = {}
libraries = {}

for line in sys.stdin:
    line = line.strip()
    key = line.split('\t')[0]
    value = line.split('\t')[1]
    if key in user_library:
        user_library[key][0] += int(value)
        user_library[key][1] += 1
    else:
        user_library[key] = [int(value),1]
for key,value in user_library.iteritems():
    activity = float(value[0])/float(value[1]);
    print key + ',' + str(activity)