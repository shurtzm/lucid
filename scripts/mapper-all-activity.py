#!/usr/bin/env python

from operator import itemgetter
import sys

libraries = {}
user_library = {}

#open action table
with open("/Users/matt/Documents/lucid/data/capstone/action_table.csv") as file:
	for line in file:
	    line_data = line.strip().split(",")
	    libraries[line_data[0]] = line_data[1]
for line in sys.stdin:
    action_detail = line.split(",")
    if action_detail[0] == "user_id":
        continue

    library = libraries["\""+action_detail[3]+"\""]
    level = action_detail[1]
    # Key is userid, library, and subscription level.
    key = action_detail[0] + "," + library + "," + level
    # Value is the day and the count for that day.
    value = [action_detail[2],action_detail[4]]
    # Print key and then count as value
    print key + '\t' + ",".join(value)