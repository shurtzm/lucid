#!/usr/bin/env python

from operator import itemgetter
import sys

user_library = {}
user_dates = {}
libraries = {}
for line in sys.stdin:
    # 3 part key, userid, library, subscription level. Value is an array that has a day and a count for that day.
    key_value = line.strip().split('\t')
    key = key_value[0]
    value = key_value[1]
    day_value = value.split(',')[0]
    count_value = value.split(',')[1]
    # Pull out the library
    library = key.split(",")[1]
    if key in user_library:
        user_library[key][0] += int(count_value)
        if day_value not in user_dates[key]:
            user_library[key][1] += 1
        else:
            user_dates[key].append(day_value)
    else:
        user_library[key] = [int(count_value),1]
        user_dates[key] = [day_value]
    # if library not in libraries:
    #     libraries[library] = [user_library[key_value[0]][0]/user_library[key_value[0]][1],user_library[key_value[0]][0]/user_library[key_value[0]][1]]
    # elif user_library[key_value[0]][0]/user_library[key_value[0]][1]<libraries[library][0]:
    #     libraries[library][0] = user_library[key_value[0]][0]/user_library[key_value[0]][1]
    # elif user_library[key_value[0]][0]/user_library[key_value[0]][1]>libraries[library][1]:
    #     libraries[library][1] = user_library[key_value[0]][0]/user_library[key_value[0]][1]

for key,value in user_library.iteritems():
    library = key.split(",")[1]
    value[0] = float(float(value[0])/float(value[1]))
    # value[0] = value[0]/libraries[library][1]
    print key + "," + str(value[0])