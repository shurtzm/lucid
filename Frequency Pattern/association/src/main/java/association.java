import java.util.Arrays;
import java.util.List;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import org.apache.spark.mllib.fpm.AssociationRules;
import org.apache.spark.mllib.fpm.FPGrowth;
import org.apache.spark.mllib.fpm.FPGrowthModel;

public class association {
    public static void main(String[] args) {

      SparkConf conf = new SparkConf().setAppName("Association");
      JavaSparkContext sc = new JavaSparkContext(conf);

      JavaRDD<String> data = sc.textFile("Documents/lucid/frequent_patern/input.csv");

      JavaRDD<List<String>> transactions = data.map(
        new org.apache.spark.api.java.function.Function<String, List<String>>() {
          public List<String> call(String line) {
            String[] parts = line.split(",");
            return Arrays.asList(parts);
          }
        }
      );

      FPGrowth fpg = new FPGrowth()
      .setMinSupport(0.1);
      //  .setNumPartitions(10);
      FPGrowthModel<String> model = fpg.run(transactions);

      for (FPGrowth.FreqItemset<String> itemset: model.freqItemsets().toJavaRDD().collect()) {
        System.out.println("[" + itemset.javaItems() + "], " + itemset.freq());
      }

      double minConfidence = 0.5;
      for (AssociationRules.Rule<String> rule
        : model.generateAssociationRules(minConfidence).toJavaRDD().collect()) {
        System.out.println(
          rule.javaAntecedent() + " => " + rule.javaConsequent() + ", " + rule.confidence());
      }

    }
}