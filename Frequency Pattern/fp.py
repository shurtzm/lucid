from pyspark.mllib.fpm import FPGrowth

data = sc.textFile("input.csv")
transactions = data.map(lambda line: line.strip().split(','))
model = FPGrowth.train(transactions, minSupport=0.1, numPartitions=-1)
result = model.freqItemsets().collect()
for fi in result:
    print(fi)